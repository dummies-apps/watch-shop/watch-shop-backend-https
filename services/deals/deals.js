const express = require("express"),
  getContent = require("../../utils/jsonFileUtils"),
  router = express(),
  winston = require("../../config/winston");

router.get("/deals", function(req, res) {
  winston.debug(`Deals service request`);
  res.status(200).send(getContent("deals"));
});

module.exports = router;
