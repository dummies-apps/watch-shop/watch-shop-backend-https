# Watch Shop Backend HTTPS

This project will provide some rest services to the frontend application.

- [frontend](https://gitlab.com/dummies-apps/watch-shop/watch-shop-frontend)

There's two services :

- Deals
- Gifts

You can use these services in the frontend or any other app. This app will provide HTTP and HTTPS responses.

## Development prerequisites

for development, you will need :

### Docker

In order to run this application as a container you'll need docker installed.

- [Windows](https://docs.docker.com/windows/started)
- [OS X](https://docs.docker.com/mac/started/)
- [Linux](https://docs.docker.com/linux/started/)

### Node

You need to install node in your environment to build and run the application.

[Node](https://nodejs.org/en/) is really easy to install & now include [NPM](https://npmjs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v0.10.24

    $ npm --version
    1.3.21

#### Node installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

## Install

Checkout the project :

    $ git clone https://gitlab.com/dummies-apps/watch-shop/watch-shop-backend-https.git
    $ cd watch-shop-backend-https

You need to install the dependencies :

    $ npm install

Check the .env file to change the app configuration.

Start the server :

    $ node app.js

### SSL Certificate

You can find the ssl certificate under the ssl directory. To generate a new certificate, use this command :

    $ openssl genrsa 2048 > private.pem && openssl req -x509 -days 1000 -new -key private.pem -out public.pem

Type Enter to answer questions, you don't need to answer them all.

This is a self signed certificate so you must install it otherwise your browser will block the communication.

## Tests

You can check the api responses by running these these commands (I will use curl but you can use postman if you want) :

    $ curl http://localhost:8090/

response :

```json
{
  "status": "UP",
  "route": "http://localhost:8090/"
}
```

    $ curl --insecure https://localhost:4300/

response :

```json
{
  "status": "UP",
  "route": "https://localhost:4300/"
}
```

    $ curl --insecure https://localhost:4300/healthz

response :

```json
{
  "status": "UP",
  "route": "https://localhost:4300/healthz"
}
```

    $ curl --insecure https://localhost:4300/deals

response :

```json
[
  {
    "name": "Casio F91W-1",
    "price": "23$",
    "description": "A tried and true style great for casual wear. With its daily alarm, hourly time signal and auto calendar, you'll never need to worry about missing an appointment again. Black Casual Classic watch with a Resin Band.",
    "image": "https://www.presse-citron.net/wordpress_prod/wp-content/uploads/2019/12/applewatchseries4.jpg",
    "details": {
      "Weight": "0.64 ounces",
      "Manufacturer": "Casio",
      "Dimensions": "3 x 3 x 4 inches"
    }
  },
  {
    "name": "LIGE Watches for Men Waterproof ",
    "price": "83$",
    "description": "Mens Watches, LIGE Watches for Men Waterproof Fashion Business Casual Sport Stainless Steel Quartz Wristwatch for Men Date Chronograph.",
    "image": "https://static.boutique.orange.fr/media-cms/mediatheque/252x252-watch-2-vue-1-89513.jpg",
    "details": {
      "Model Year": "2019",
      "Item Shape": "Round",
      "Display Type": "Analog",
      "Diameter": "1.73 inches"
    }
  },
  {
    "name": "LIGE Watches Mens Luxury",
    "price": "29.99$",
    "description": "LIGE Watches Mens Luxury Stainless Steel Waterproof Analog Quartz Watch with Chronograph Business Dress Wrist Watch.",
    "image": "https://www.dhresource.com/600x600/f2/albu/g9/M01/8F/B7/rBVaVVzb56OAMCPMAAEyu6xUgug628.jpg",
    "details": {
      "Bezel material": "Metal",
      "Band Color": "Silver",
      "Band width": "0.87 inches"
    }
  },
  {
    "name": "KONXIDO",
    "price": "14.99$",
    "description": "Mens Quartz Watch, Business Casual Fashion Analog Wrist Watch Classic Calendar Date Window, Waterproof 30M Water Resistant Comfortable Genuine Leather Watches.",
    "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLCdhDx_C_q-izZEh9UWI01U-ypwIJtcoQG1Q22HsneSv0T7ZL&s",
    "details": { "Diameter": "1.81 inches", "Band length": "Men's Standard" }
  },
  {
    "name": "LEVOV",
    "price": "$399.99",
    "description": "Levov Watch - Stainless Steel Mens Watch - Swiss Movement Quartz Watch for Men - 38mm Stainless Steel Case with Blue Dial - 18mm Watch Band.",
    "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQqLn5Y9B7bRMn-MeEerryuS1iA-mWTtltrW0wHOICN8pWfKC8b&s",
    "details": { "Display Type": "Analog", "Case material": "Stainless steel" }
  },
  {
    "name": "Welly Merck",
    "price": "109$",
    "description": "Welly Merck Leather Watch Men Minimalistic Watch Luxury Design Swiss Movement Sapphire Crystal Stainless Steel Analog Wrist Watch with Interchangeable Strap.",
    "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSu1Z7GpMnLKTpPHQfeYhrSMonKEXKbk2gPAAm3ftXExyiyiDpy&s",
    "details": {
      "Item Shape": "Round",
      "Display Type": "Analog",
      "Manufacturer": "WM WELLY MERCK"
    }
  },
  {
    "name": "Stuhrling Original",
    "price": "54.95$",
    "description": "Stuhrling Original Mens Watch Calfskin Leather Strap - Dress + Casual Design - Analog Watch Dial with Date, 3997Z Watches for Men Collection.",
    "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTePAO4aW3mld5W9WURMt_haooTNMhkLbnOoq7ZvKEbDj5hr4G&s",
    "details": {
      "Model number": "3997Z.4",
      "Manufacturer": "Stuhrling Original",
      "Model Year": "2018",
      "Band Material": "Leather",
      "Band width": "20 millimeters",
      "Band Color": "Black",
      "Dial color": "Gray"
    }
  },
  {
    "name": "SONGDU Men's Fashion",
    "price": "235.55$",
    "description": "SONGDU Men's Fashion Date Slim Analog Quartz Watches with Stainless Steel Mesh Band.",
    "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJA5pGSuj51sf9IDULsCX_H9j_0ACTAt5ff7iGgdGPJjsyhNhK&s",
    "details": { "Case material": "Stainless steel", "Manufacturer": "SONGDU" }
  },
  {
    "name": "SONGDU",
    "price": "25.78$",
    "description": "SONGDU Men's Ultra-Thin Quartz Analog Date Wrist Watch with Black Leather Strap.",
    "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT5SglMo-Ecjk3cyVd11Ce8jgeeuru_nDf3SlahAiEhAnkNPl5k&s",
    "details": { "Seller": "SONGDU", "Item Shape": "Round" }
  },
  {
    "name": "CFGem",
    "price": "40.86$",
    "description": "Black Minimal/Contracted Style Analog Automatic Wrist Watch for Men.",
    "image": "https://www.gulmor.in/wp-content/uploads/2019/05/910D0vQR2HL._UL1500_.jpg",
    "details": { "Band Color": "Black", "Manufacturer": "CFGem" }
  }
]
```

    $ curl --insecure https://localhost:4300/gifts

response :

```json
[
  {
    "name": "Happy Birthday",
    "price": "25$",
    "description": "Amazon.com eGift Card.",
    "image": "https://m.media-amazon.com/images/I/81gQraBrBZL._SS500_.jpg",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "Yes",
      "Delivery method": "Email",
      "Gift amount": "$1 - $2,000"
    }
  },
  {
    "name": "Welcome Baby",
    "price": "15$",
    "description": "Amazon.com eGift Card.",
    "image": "https://cdn5.vectorstock.com/i/1000x1000/35/79/welcome-baby-card-vector-14713579.jpg",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "No",
      "Delivery method": "Mail",
      "Gift amount": "$1 - $4,000"
    }
  },
  {
    "name": "Congratulations",
    "price": "7$",
    "description": "Amazon.com eGift Card.",
    "image": "https://cdn.shopify.com/s/files/1/0786/6519/products/congrats_on_sleeping_with_the_same_person_wedding_card.jpg?v=1532655560",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "Yes",
      "Delivery method": "PDF download",
      "Gift amount": "$1 - $6,000"
    }
  },
  {
    "name": "Thank you",
    "price": "6.99$",
    "description": "Amazon.com eGift Card.",
    "image": "https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto/gigs/108897597/original/2a21b24798a3c3fe4a492ff52ab8ec52b86bc71b/design-awesome-thank-you-card-within-6-hours.jpg",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "Yes",
      "Delivery method": "Email",
      "Gift amount": "$1 - $200"
    }
  }
]
```

## Docker Image

To build and run the docker image, use this command :

    $ docker build -t mouhamedali/watch-shop-backend-https .

If you want to run the app without cloning the repo, use this command :

    $ docker pull mouhamedali/watch-shop-backend-https .

Now you can run the app :

    $ docker run -d -p 8080:80 -p 4300:443 mouhamedali/watch-shop-backend-https

and make some tests :

    $ curl http://localhost:8080/

response :

```json
{
  "status": "UP",
  "route": "http://localhost:8080/"
}
```

    $ curl --insecure https://localhost:4300/

response :

```json
{
  "status": "UP",
  "route": "https://localhost:4300/"
}
```

---

## Authors

- Mohamed Ali AMDOUNI
