const express = require("express"),
  app = express(),
  winston = require("./config/winston"),
  fs = require("fs"),
  cors = require("cors"),
  https = require("https");
require("dotenv").config();

// enable all cors
app.use(cors());

//get application context
var baseURL = process.env.BASE_URL;

app.use(baseURL, require("./services/common/healthCheck"));
app.use(baseURL, require("./services/deals/deals"));
app.use(baseURL, require("./services/gifts/gifts"));

const httpPort = process.env.HTTP_PORT || 8080;
app.listen(httpPort, function() {
  winston.debug(
    "Http Server %d is listening on port %d",
    process.pid,
    httpPort
  );
});

const httpsPort = process.env.HTTPS_PORT || 4300;
https
  .createServer(
    {
      key: fs.readFileSync("ssl/private.pem", "utf8"),
      cert: fs.readFileSync("ssl/public.pem", "utf8")
    },
    app
  )
  .listen(httpsPort, () =>
    winston.debug(
      "Https Server %d is listening on port %d",
      process.pid,
      httpsPort
    )
  );
